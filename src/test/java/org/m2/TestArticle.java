package org.m2;

import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestArticle {
    public static final String host = "redis://localhost";
    public RedisClient redisClient;
    public StatefulRedisConnection<String, String> connection;
    public RedisCommands<String, String> cmd;

    @BeforeEach
    public void cleanUpDB() {
        redisClient = RedisClient.create(host);
        connection = redisClient.connect();
        connection.flushCommands();
        cmd = connection.sync();
        cmd.flushall();
    }

    @AfterEach
    public void closeDB() {
        connection.close();
        redisClient.shutdown();
    }

    @Test
    public void testAddArticle() {
        Article article = new Article();
        String id = article.ajoutArticle(
            cmd,
            "Paul",
            "l'étrange noel de mr jack",
            "url",
            "horreur"
        );
        assertEquals(id,"1");
    }

    @Test
    public void testRecupererArticle() {
        Article article = new Article();
        String id = article.ajoutArticle(
            cmd,
            "Paul",
            "l'étrange noel de mr jack",
            "url",
            "horreur"
        );
        HashMap<String, String> articleRecupere = article.recupererArticle(cmd, id);
        assertEquals(articleRecupere.get("titre"), "l'étrange noel de mr jack");
        assertEquals(articleRecupere.get("utilisateur"), "Paul");
        assertEquals(articleRecupere.get("url"), "url");
        assertEquals(articleRecupere.get("categorie"), "horreur");
    }

    @Test
    public void testGetAllArticles() {
        Article article = new Article();
        article.ajoutArticle(
            cmd,
            "Paul",
            "l'étrange noel de mr jack",
            "url",
                "horreur"
        );
        article.ajoutArticle(
            cmd,
            "Paul",
            "l'étrange noel de mr jack",
            "url",
                "horreur"
        );
        String[] articles = article.recupererTousLesArticles(cmd);
        assertEquals(articles.length,2);
    }

    @Test
    public void testVoterPourArticle() {
        Article article = new Article();
        String id = article.ajoutArticle(
            cmd,
            "Paul",
            "l'étrange noel de mr jack",
            "url",
                "horreur"
        );

        article.voterPourArticle(cmd, "Paul", id);
        HashMap<String, String> articleMap = article.recupererArticle(cmd, id);
        assertEquals(articleMap.get("nbvotes"),"1");

        article.voterPourArticle(cmd, "Paul", id);
        articleMap = article.recupererArticle(cmd, id);
        assertEquals(articleMap.get("nbvotes"),"1");

        article.voterPourArticle(cmd, "Liv", id);
        articleMap = article.recupererArticle(cmd, id);
        assertEquals(articleMap.get("nbvotes"),"2");
    }

    @Test
    public void testRecupererArticlesAvecLePlusGrosScore() {
        Article article = new Article();
        String id1 = article.ajoutArticle(cmd,"Paul","jack","url", "horreur");
        String id2 = article.ajoutArticle(cmd,"Paul","jack","url", "horreur");
        article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");
        article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");
        article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");
        article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");
        article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");
        article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");
        String id9 = article.ajoutArticle(cmd,"Paul","jack","url", "horreur");
        article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");
        article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");
        article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");
        article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");
        String id14 = article.ajoutArticle(cmd,"Paul","jack","url", "horreur");
        article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");

        // vote 10 times for the first article by 10 different users
        for (int i = 0; i < 10; i++) {
            article.voterPourArticle(cmd, "user_" + i, id1);
        }

        // vote 9 times for the 14th article by 9 different users
        for (int i = 0; i < 9; i++) {
            article.voterPourArticle(cmd, "user_" + i, id14);
        }

        // vote 8 times for the 9th article by 8 different users
        for (int i = 0; i < 8; i++) {
            article.voterPourArticle(cmd, "user_" + i, id9);
        }

        // vote 7 times for the 2nd article by 7 different users
        for (int i = 0; i < 7; i++) {
            article.voterPourArticle(cmd, "user_" + i, id2);
        }


        String [] articles = article.recupererArticlesAvecLePlusGrosScore(cmd);
        assertEquals(10, articles.length);
        assertEquals("article:"+id1, articles[0]);
        assertEquals("article:"+id14, articles[1]);
        assertEquals("article:"+id9, articles[2]);
        assertEquals("article:"+id2, articles[3]);
    }

    @Test
    public void testRecupererArticlesLePlusVote() {
        Article article = new Article();
        String id1 = article.ajoutArticle(cmd,"Paul","jack","url", "horreur");
        String id2 = article.ajoutArticle(cmd,"Paul","jack","url", "horreur");
        String id3 = article.ajoutArticle(cmd,"Paul","jack","url", "horreur");
        article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");
        article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");
        article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");
        article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");
        article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");
        article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");
        article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");
        article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");
        article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");
        article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");
        article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");
        article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");

        article.voterPourArticle(cmd, "Liv", id1);
        article.voterPourArticle(cmd, "Ben", id1);
        article.voterPourArticle(cmd, "Tom", id1);
        article.voterPourArticle(cmd, "Perrin", id1);

        article.voterPourArticle(cmd, "Liv", id2);
        article.voterPourArticle(cmd, "Ben", id2);

        article.voterPourArticle(cmd, "Liv", id3);

        String [] articles = article.recupererArticlesLePlusVote(cmd);
        assertEquals(articles.length,10);
        assertEquals(articles[0].replace("article:", ""),id1);
        assertEquals(articles[1].replace("article:", ""),id2);
        assertEquals(articles[2].replace("article:", ""),id3);
    }


    @Test
    public void testRecupererArticlesParCategorie() {
        Article article = new Article();
        article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");
        article.ajoutArticle(cmd, "Paul", "massacre", "url", "horreur");
        article.ajoutArticle(cmd, "Paul", "du sang partout", "url", "horreur");
        article.ajoutArticle(cmd, "Paul", "rigolo", "url", "comedy");
        article.ajoutArticle(cmd, "Paul", "c'est marrant", "url", "comedy");
        article.ajoutArticle(cmd, "Paul", "final fantasy", "url", "fantasy");

        String [] articles = article.recupererArticlesCategorie(cmd, "horreur");
        assertEquals(articles.length,3);

        articles = article.recupererArticlesCategorie(cmd, "comedy");
        assertEquals(articles.length,2);

        articles = article.recupererArticlesCategorie(cmd, "fantasy");
        assertEquals(articles.length,1);
    }

    @Test
    public void testrecupererScoresArticlesCategorie() {
        Article article = new Article();
        String id1 = article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");
        String id2 = article.ajoutArticle(cmd, "Paul", "massacre", "url", "horreur");
        String id3 = article.ajoutArticle(cmd, "Paul", "du sang partout", "url", "horreur");

        article.voterPourArticle(cmd, "Liv", id1);
        article.voterPourArticle(cmd, "Perrin", id3);

        HashMap<String, Double> scores = article.recupererScoresArticlesCategorie(cmd, "horreur");
        assertEquals(scores.size(), 3);
        HashMap<String, String> art1 = article.recupererArticle(cmd, id1);
        assertEquals(
                scores.get("article:"+id1),
                Double.parseDouble(art1.get("time")) + (Double.parseDouble(art1.get("nbvotes")) * Article.VOTE_POIDS)
        );

        HashMap<String, String> art2 = article.recupererArticle(cmd, id2);
        assertEquals(
                scores.get("article:"+id2),
                Double.parseDouble(art2.get("time")) + (Double.parseDouble(art2.get("nbvotes")) * Article.VOTE_POIDS)
        );

        HashMap<String, String> art3 = article.recupererArticle(cmd, id3);
        assertEquals(
                scores.get("article:"+id3),
                Double.parseDouble(art3.get("time")) + (Double.parseDouble(art3.get("nbvotes")) * Article.VOTE_POIDS)
        );
    }

    @Test
    public void testRecupererArticleLePlusRecent() {
        Article article = new Article();
        String id1 = article.ajoutArticle(cmd,"Paul","jack","url", "horreur");
        String id2 = article.ajoutArticle(cmd,"Paul","massacre","url", "horreur");
        String id3 = article.ajoutArticle(cmd,"Paul","du sang partout","url", "horreur");
        String id4 = article.ajoutArticle(cmd,"Paul","rigolo","url", "comedy");
        String id5 = article.ajoutArticle(cmd,"Paul","c'est marrant","url", "comedy");
        String id6 = article.ajoutArticle(cmd,"Paul","final fantasy","url", "fantasy");

        String [] articles = article.recupererArticlesLePlusRecent(cmd);
        assertEquals(articles.length,6);
        assertEquals("article:"+id6, articles[0]);
        assertEquals("article:"+id5, articles[1]);
        assertEquals("article:"+id4, articles[2]);
        assertEquals("article:"+id3, articles[3]);
        assertEquals("article:"+id2, articles[4]);
        assertEquals("article:"+id1, articles[5]);
    }

    @Test
    public void testGroupe() {
        Article article = new Article();
        String id1 = article.ajoutArticle(cmd, "Paul", "jack", "url", "horreur");
        String id2 = article.ajoutArticle(cmd, "Paul", "massacre", "url", "horreur");
        String id3 = article.ajoutArticle(cmd, "Paul", "du sang partout", "url", "horreur");
        String id4 = article.ajoutArticle(cmd, "Paul", "rigolo", "url", "comedy");
        String id5 = article.ajoutArticle(cmd, "Paul", "c'est marrant", "url", "comedy");

        article.ajouterArticleDansUnGroupe(cmd, id1, "groupe1");
        article.ajouterArticleDansUnGroupe(cmd, id2, "groupe1");
        article.ajouterArticleDansUnGroupe(cmd, id3, "groupe1");
        article.ajouterArticleDansUnGroupe(cmd, id4, "groupe2");
        article.ajouterArticleDansUnGroupe(cmd, id5, "groupe2");

        String[] articles = article.recupererArticlesGroupe(cmd, "groupe1");
        assertEquals(articles.length, 3);

        articles = article.recupererArticlesGroupe(cmd, "groupe2");
        assertEquals(articles.length, 2);

        article.supprimerArticleDunGroupe(cmd, id1, "groupe1");
        articles = article.recupererArticlesGroupe(cmd, "groupe1");
        assertEquals(articles.length, 2);
    }
}