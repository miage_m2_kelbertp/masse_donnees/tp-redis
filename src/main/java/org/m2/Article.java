package org.m2;

import io.lettuce.core.api.sync.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Article {

    private static final int UNE_SEMAINE = 7;
    public static final long VOTE_POIDS = 197;

    public String ajoutArticle(RedisCommands<String, String> sync,
                               String utilisateur,
                               String titre, String url, String categorie) {
        String articleId = String.valueOf(sync.incr("article:"));
        String articleSelectionne = "selectionne:" + articleId;
        sync.sadd(articleSelectionne, utilisateur);
        sync.expire(articleSelectionne, UNE_SEMAINE);
        long now = System.currentTimeMillis() / 1000;
        String article = "article:" + articleId;
        HashMap<String,String> donnees = new HashMap<>();
        donnees.put("titre", titre);
        donnees.put("url", url);
        donnees.put("utilisateur", utilisateur);
        donnees.put("categorie", categorie);
        donnees.put("nbvotes", "1");
        sync.hmset(article, donnees);

        sync.zadd("score", now + VOTE_POIDS, article);
        sync.zadd("time", now, article);
        return articleId;
    }

    public String[] recupererTousLesArticles(RedisCommands<String, String> sync) {
        String[] articles = sync.keys("article:*").toArray(new String[0]);
        List<String> list = new ArrayList<>(Arrays.asList(articles));
        list.remove("article:");
        return list.toArray(new String[0]);
    }

    public void voterPourArticle(RedisCommands<String, String> sync,
                                 String utilisateur,
                                 String articleId) {
        String articleSelectionne = "selectionne:" + articleId;
        if (sync.sadd(articleSelectionne, utilisateur) == 1) {
            sync.expire(articleSelectionne, UNE_SEMAINE);
            String article = "article:" + articleId;
            sync.hincrby(article, "nbvotes", 1);
            sync.zincrby("score", VOTE_POIDS, article);
        }
    }

    public HashMap<String,String> recupererArticle(RedisCommands<String, String> sync, String articleId) {
        String article = "article:" + articleId;
        Double time = sync.zscore("time", article);
        Double score = sync.zscore("score", article);
        HashMap<String,String> donnees = new HashMap<>();
        donnees.put("time", String.valueOf(time));
        donnees.put("score", String.valueOf(score));
        donnees.put("nbvotes", sync.hget(article, "nbvotes"));
        donnees.put("titre", sync.hget(article, "titre"));
        donnees.put("url", sync.hget(article, "url"));
        donnees.put("utilisateur", sync.hget(article, "utilisateur"));
        donnees.put("categorie", sync.hget(article, "categorie"));
        return donnees;
    }

    public String[] recupererArticlesLePlusVote(RedisCommands<String, String> sync) {
        String[] articles = this.recupererTousLesArticles(sync);
        HashMap<String, Integer> votes = new HashMap<>();
        for (String article : articles) {
            votes.put(article, Integer.parseInt(sync.hget(article, "nbvotes")));
        }

        String[] articlesPlusVotes = new String[10];
        for (int i = 0; i < 10; i++) {
            String articlePlusVote = null;
            for (String article : votes.keySet()) {
                if (articlePlusVote == null || votes.get(article) > votes.get(articlePlusVote)) {
                    articlePlusVote = article;
                }
            }
            if (articlePlusVote != null) {
                articlesPlusVotes[i] = articlePlusVote;
                votes.remove(articlePlusVote);
            }
        }
        return articlesPlusVotes;
    }



    public String[] recupererArticlesAvecLePlusGrosScore(RedisCommands<String, String> sync) {
        return sync.zrevrange("score", 0, 9).toArray(new String[0]);
    }

    public String[] recupererArticlesLePlusRecent(RedisCommands<String, String> sync) {
        return sync.zrevrange("time", 0, 9).toArray(new String[0]);
    }

    public String[] recupererArticlesCategorie(RedisCommands<String, String> sync, String categorie) {
        String[] articles = this.recupererTousLesArticles(sync);
        List<String> articlesCategorie = new ArrayList<>();
        for (String article : articles) {
            if (sync.hget(article, "categorie").equals(categorie)) {
                articlesCategorie.add(article);
            }
        }

        return articlesCategorie.toArray(new String[0]);
    }

    public HashMap<String, Double> recupererScoresArticlesCategorie(RedisCommands<String, String> sync, String categorie) {
        String[] articles = this.recupererArticlesCategorie(sync, categorie);
        HashMap<String, Double> scores = new HashMap<>();
        for (String article : articles) {
            scores.put(article, sync.zscore("score", article));
        }
        return scores;
    }

    public void ajouterArticleDansUnGroupe(RedisCommands<String, String> sync, String articleId, String groupeName) {
        String article = "article:" + articleId;
        sync.sadd("groupe:" + groupeName, article);
    }

    public String[] recupererArticlesGroupe(RedisCommands<String, String> sync, String groupeName) {
        return sync.smembers("groupe:" + groupeName).toArray(new String[0]);
    }

    public void supprimerArticleDunGroupe(RedisCommands<String, String> sync, String articleId, String groupeName) {
        String article = "article:" + articleId;
        sync.srem("groupe:" + groupeName, article);
    }
}
