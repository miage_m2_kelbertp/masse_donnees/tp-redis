# TP REDIS - KELBERT Paul

## Partie 3

### 

### a. Quelle structure de Redis vous paraît être la plus appropriée ?

Le Hash serait la meilleure option pour ce type de données. Ainsi, sur un même objet, nous pourrions avoir plusieurs attributs sur un même objet.

### b. Quelles structures de Redis vous paraissent les plus adaptées pour ces deux besoins ?

Le Sorted Set serait le plus adapté.

### c. Même question que précédemment concernant la structure.

Il semblerait que le Set soit une bonne idée.
